""" 
    This script contains the calling of functions of the script "assembly_functions.py" for the generation of simulations 
    and the detection and tracking of the cell assembly simulations. 
    The code was written by Vítor Lopes dos Santos, some parameters were modified by Dorottya Kiss.
"""

import assembly_functions as assembly
import time as time
import numpy as np
import matplotlib.pyplot as plt

# membership of assemblies
membership = [[0, 1, 2],\
              [2, 3, 4, 5],\
              [0, 5, 6, 7]]

# activation rate of each assembly (activation/bins)
actrate = [.06,.05,.04]

# activation strength of each assembly 
actstrength = [5.,4.,4.5]

# creating an object with assembly definitions
assemblies = assembly.artificial_assemblies(membership = membership, actrate = actrate, actstrength=actstrength)

# total number of neurons in the simulation
nneurons = 25 

# total number of time bins
nbins = 10000 

# mean baseline rate of all neurons
rate = 1.

# calling activation matrix generation function
actmat = assembly.artificial_actmat_generation(assemblies, nneurons = nneurons, nbins=nbins, rate = rate)

# plotting the first 50 time bins of the simulation
plt.imshow(actmat[:,0:50])

##############################################################################

### using Marcenko-Pastur function as null distribution
# getting the actual epoch time
t0 = time.time()
# calling the runPatterns function
patterns,significance,zactmat = assembly.runPatterns(actmat,method='ica',nullhyp='mp')
# calculating the running time
tf = np.round(1000*(time.time()-t0))/1000
# plotting the results of the PCA + MP distribution based assembly extraction
plt.figure(figsize=(15,5))
for (pi,pattern) in enumerate(patterns):
        plt.subplot(1,len(patterns),1+pi)
        plt.stem(pattern)
        plt.xlabel('Neuron #')
        plt.ylabel('Weight [-]')
plt.suptitle('Using MP distribution, elapsed time: '+str(tf)+' seconds')


### using simple bin shuffling for computing null distribution
# defining the number of controls
nshu = 1000
# defining which percentile of control distribution will be used as significance threshold
percentile = 99.5
t0 = time.time()
patterns,significance,zactmat = assembly.runPatterns(actmat,method='ica',nullhyp='bin',nshu=nshu,percentile=percentile)
tf = np.round(1000*(time.time()-t0))/1000 
# plotting the results of the PCA + bin shuffling based assembly extraction
plt.figure(figsize=(15,5))
for (pi,pattern) in enumerate(patterns): 
        plt.subplot(1,len(patterns),1+pi)
        plt.stem(pattern)
        plt.xlabel('Neuron #')
        plt.ylabel('Weight [-]')
plt.suptitle('Using bin shuffling, elapsed time: '+str(tf)+' seconds')


### using circular shuffling for computing null distribution
nshu = 1000
percentile = 99.5
t0 = time.time()
patterns,significance,zactmat = assembly.runPatterns(actmat,method='ica',nullhyp='circ',nshu=nshu,percentile=percentile)
tf = np.round(1000*(time.time()-t0))/1000 
# plotting the results of the PCA + circle shuffling based assembly extraction
plt.figure(figsize=(15,5))
for (pi,pattern) in enumerate(patterns):
        plt.subplot(1,len(patterns),1+pi)
        plt.stem(pattern)
        plt.xlabel('Neuron #')
        plt.ylabel('Weight [-]')
plt.suptitle('Using circular shuffling, elapsed time: '+str(tf)+' seconds')



# calling the assembly activity computing function
assemblyAct = assembly.computeAssemblyActivity(patterns,zactmat)

# subplotting the first 150 time bins of the z-scored spike matrix
plt.figure(figsize=(15,6))
s1 = plt.subplot(211)
plt.imshow(zactmat[:,0:150],cmap='Reds',interpolation='nearest',aspect='auto')
plt.title('z-scored activity matrix')
plt.ylabel('Neuron #')

# subplotting the activity of the assemblies in the first 150 time bins
plt.subplot(212,sharex=s1)
plt.plot(assemblyAct[:,0:150].T,linewidth=4)
plt.title('Assembly activities over time')
plt.xlim(0,149)
plt.xlabel('Time [bin]')
plt.ylabel('Strength [-]')
plt.tight_layout()
