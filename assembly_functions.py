""" 
    This script contains the callable functions for the scripts "assembly_detection_real.py" and 
    "assembly_detection_sim.py" for the generation of simulations and the detection and tracking of the cell assembly simulations. 
    The code was written by Vítor Lopes dos Santos.
"""

from sklearn.decomposition import PCA
from scipy import stats
import numpy as np
from numpy import matlib as mb
np.matlib = mb

# generates the activation matrix:
def artificial_actmat_generation(assemblies, nneurons, nbins, rate): 

        # it initializes the pseudorandom number generator
        np.random.seed() 
        
        # generating the initial activation matrix with neurons around 'rate' with Poisson distribution
        actmat = np.random.poisson(rate,nneurons*nbins).reshape(nneurons,nbins) 
        
        # creates activation bins list for the 'artificial_assemblies' type object
        assemblies.actbins = [None]*len(assemblies.membership) # [None, None, None]
        
        # iterating through the members in the assemblies
        for (ai,members) in enumerate(assemblies.membership):
            
                # creates a vector for every assembly with the indeces of its members
                members = np.array(members)
                
                # calculating the number of active bins for the assembly
                nact = int(nbins*assemblies.actrate[ai])
                
                # calculating the activation strength for the assembly
                actstrength_ = rate*assemblies.actstrength[ai]
                
                # randomly choosing the activation bins for the assembly (puts their indeces in a column vector)
                actbins = np.argsort(np.random.rand(nbins))[0:nact]
                
                # overrides the selected activation bins of the activation matrix, with an increased activation strength
                actmat[members.reshape(-1,1),actbins] = np.ones((len(members),nact))+actstrength_
                
                # fills up the empty actbins list with the selected ones, sorted in order
                assemblies.actbins[ai] = np.sort(actbins)

        return actmat

class artificial_assemblies:
        def __init__(self, membership, actrate, actstrength):

                self.membership  = membership
                self.actrate     = actrate
                self.actstrength = actstrength

# calculates the threshold for defining the number of cell assemblies:
def marcenkopastur(significance): 
        
        # number of bins
        nbins = significance.nbins
        # number of neurons
        nneurons = significance.nneurons
        tracywidom = significance.tracywidom # bool, 'false'
    
        # calculates statistical threshold from Marcenko-Pastur distribution
        q = float(nbins)/float(nneurons) # (silent neurons are counted too)

        # calculating lambdaMax - since sigma is 1, we can skip it
        lambdaMax = pow((1+np.sqrt(1/q)),2)
        # opportunity to apply Tracy-Widom correction, which is a finite sample bias correction method
        # here it is not applied since its value is False
        lambdaMax += tracywidom*pow(nneurons,-2./3)
        
        return lambdaMax
    
# calculates the maximum of the variances - i.e. the maximal eigenvalues:
def getlambdacontrol(zactmat_): 
    
        significance_ = PCA()
        # fits the model with the transposed zactmat_
        significance_.fit(zactmat_.T)
        lambdamax_ = np.max(significance_.explained_variance_)
        # in this framework each surrogate matrix contributes its maximal eigenvalue to the chance distribution, which is
        # a more conservative approach than considering all surrogate eigenvalues (as in case of M-P distr.)
        
        return lambdamax_
    
# randomly mixes the time bins of the neurons in a random order:
def binshuffling(zactmat,significance): 
    
        np.random.seed()
        # nshu is the number of shuffling controls
        lambdamax_ = np.zeros(significance.nshu)
        for shui in range(significance.nshu):
                zactmat_ = np.copy(zactmat)
                for (neuroni,activity) in enumerate(zactmat_):
                        # random mixes the neurons
                        randomorder = np.argsort(np.random.rand(significance.nbins))
                        zactmat_[neuroni,:] = activity[randomorder]
                # calls the lambda_max calculating function on the new, surrogate spike matrix
                lambdamax_[shui] = getlambdacontrol(zactmat_)
        # calculates the lambdaMax value that way that it is bigger than the 'percentile' % of the lambdamax_ values
        lambdaMax = np.percentile(lambdamax_,significance.percentile)
        
        return lambdaMax
    
# switches the two, randomly cut segments:
def circshuffling(zactmat,significance):
    
        np.random.seed()

        lambdamax_ = np.zeros(significance.nshu)
        for shui in range(significance.nshu):
                zactmat_ = np.copy(zactmat)
                for (neuroni,activity) in enumerate(zactmat_):
                        # randomly defines where to cut the spike train 
                        cut = int(np.random.randint(significance.nbins*2))
                        # "shifts" the spike train with the number of bins before the cut
                        zactmat_[neuroni,:] = np.roll(activity,cut)
                lambdamax_[shui] = getlambdacontrol(zactmat_)

        lambdaMax = np.percentile(lambdamax_,significance.percentile)
        
        return lambdaMax

# runs significance estimation (i.e. cell assembly number determination) for the different null hypothesis methods
def runSignificance(zactmat,significance):
    
        if significance.nullhyp == 'mp':
                lambdaMax = marcenkopastur(significance)
        elif significance.nullhyp == 'bin':
                lambdaMax = binshuffling(zactmat,significance)
        elif significance.nullhyp == 'circ':
                lambdaMax = circshuffling(zactmat,significance)
        else: 
                print('ERROR !')
                print('null hypothesis method '+str(nullhyp)+' not understood')
                significance.nassemblies = np.nan
        
        # we determine as many assemblies as many PCs' variance is bigger than lambdaMax
        nassemblies = np.sum(significance.explained_variance_>lambdaMax)
        # give these parameters as attributumes for the significance object
        significance.nassemblies = nassemblies
        significance.lambdaMax = lambdaMax
        
        return significance
        
# assembly extraction using the solely PCA or PCA-ICA based method:
def extractPatterns(actmat,significance,method):
        nassemblies = significance.nassemblies
    
        if method == 'pca':
                idxs = np.argsort(-significance.explained_variance_)[0:nassemblies] 
                patterns = significance.components_[idxs,:]
        elif method == 'ica':
                # using the built-in fastICA algorithm
                from sklearn.decomposition import FastICA
                ica = FastICA(n_components=nassemblies)
                ica.fit(actmat.T)
                patterns = ica.components_
        else:
                print('ERROR !')
                print('assembly extraction method '+str(method)+' not understood')
                patterns = np.nan
                
        
                
        if patterns is not np.nan:
                
                patterns = patterns.reshape(nassemblies,-1)
                # sets norm of assembly vectors to 1
                norms = np.linalg.norm(patterns,axis=1)
                # pattern is a matrix which contains the weights of the neurons associated with the assemblies
                patterns /= np.matlib.repmat(norms,np.size(patterns,1),1).T
        
        return patterns

# calls previously described functions and estimates the assemblie's activity 
def runPatterns(actmat, method='ica', nullhyp = 'mp', nshu = 1000, percentile = 99, tracywidom = False):
        '''
        INPUTS
        
            actmat:     activity matrix - numpy array (neurons, time bins) 
            
            nullhyp:    defines how to generate statistical threshold for assembly detection.
                            'bin' - bin shuffling, will shuffle time bins of each neuron independently
                            'circ' - circular shuffling, will shift time bins of each neuron independently
                                                                obs: maintains (virtually) autocorrelations
                            'mp' - Marcenko-Pastur distribution - analytical threshold
                            
            nshu:       defines how many shuffling controls will be done (n/a if nullhyp is 'mp')
            
            percentile: defines which percentile to be used use when shuffling methods are employed.
                                                                        (n/a if nullhyp is 'mp')
                                                                         
            tracywidow: determines if Tracy-Widom is used. See Peyrache et al 2010.
                                                    (n/a if nullhyp is NOT 'mp')
                                                    
        OUTPUTS
            
            patterns:     co-activation patterns (assemblies) - numpy array (assemblies, neurons)
            significance: object containing general information about significance tests
            zactmat:      returns z-scored actmat
        
        '''
        # number of neurons
        nneurons = np.size(actmat,0)
        # number of bins
        nbins = np.size(actmat,1)
        
        # if a neuron is not active, it is a silent neuron
        silentneurons = np.var(actmat,axis=1)==0
        actmat_ = actmat[~silentneurons,:]
        
        # z-score normalization (of the neuron spike trains)
        zactmat_ = stats.zscore(actmat_,axis=1)
        
        # running significance (estimating number of assemblies)
        significance = PCA()
        significance.fit(zactmat_.T)
        significance.nneurons = nneurons
        significance.nbins = nbins
        significance.nshu = nshu
        significance.percentile = percentile
        significance.tracywidom = tracywidom
        significance.nullhyp = nullhyp
        significance = runSignificance(zactmat_,significance)
        if np.isnan(significance.nassemblies):
                return

        if significance.nassemblies<1:
                print('WARNING !')
                print('no assembly detected!')
                patterns = []
        else:
                # extracting co-activation patterns
                patterns_ = extractPatterns(zactmat_,significance,method)
                if patterns_ is np.nan:
                	return
		
        		# putting eventual silent neurons back (their assembly weights are defined as zero)
                patterns = np.zeros((np.size(patterns_,0),nneurons))
                patterns[:,~silentneurons] = patterns_
        zactmat = np.copy(actmat)
        zactmat[~silentneurons,:] = zactmat_
		
        return patterns,significance,zactmat

# calculates the detected assemblies' activity over time:
def computeAssemblyActivity(patterns,zactmat,zerodiag = True):

        nassemblies = len(patterns)
        nbins = np.size(zactmat,1)

        assemblyAct = np.zeros((nassemblies,nbins))
        for (assemblyi,pattern) in enumerate(patterns):
                # computes the outer product of the current pattern with itself 
                projMat = np.outer(pattern,pattern) 
                # set zeros into the main diagonal
                projMat -= True*np.diag(np.diag(projMat))
                for bini in range(nbins):
                        # multiplies together the columns of the zactmat and the elements of the projMat,
                        # then we get a column vector and multiply this with the columns of the zactmat
                        # thus we get a scalar 
                        assemblyAct[assemblyi,bini] = \
                                np.dot(np.dot(zactmat[:,bini],projMat),zactmat[:,bini])
                        
        return assemblyAct
