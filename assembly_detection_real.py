"""
    This script contains the reading process of the electrophysiological recordings and several plotting of the
    detected assemblies' pattern and activity. Written by Dorottya Kiss.
    The functions needed for assembly detection and estimation are called from the script: assembly_functions.py, 
    which was written by Vítor Lopes dos Santos, commented mainly by Dorottya Kiss.
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import assembly_functions as assembly
import seaborn as sns

########################################## READING IN DATA #####################################
# reading in the recorded data
hipp_spikes = pd.read_excel (r'C:\Users\Csucsu\anaconda3\SpTimesUTpW2418_09_hipp.xlsx')
medr_spikes = pd.read_excel (r'C:\Users\Csucsu\anaconda3\SpTimesUTpW2418_09_mrap.xlsx')
# converting dataframe to array
hippocampal_spikes = hipp_spikes.to_numpy(dtype='float32') # 10447 x 57
medianraphe_spikes = medr_spikes.to_numpy(dtype='float32') # 104447 x 42
# saving the identification number of neurons
hipp_indexes = hipp_spikes.columns
medr_indexes = medr_spikes.columns

# reading in the timestamps of the stimuli
timestamps = pd.read_excel (r'C:\Users\Csucsu\anaconda3\2418_09_Tone_TailShock.xlsx')
timestsamp_type = timestamps.columns
time_stamps = timestamps.to_numpy(dtype='float32')
tone_start_t = time_stamps[:,0]
tone_stop_p = time_stamps[:,1]
tail_shock_k = time_stamps[:,2]

######################################### BINNING - generating spike matrices ####################################

from_where = 115 # starting point of the detection (after cell labelling)
until_when = 1440 # ending point of the detection (before cell labelling)
window_size = 0.025 # arbitrary time bin width (in seconds)
tone_start = (tone_start_t - float(from_where))/window_size # converting the real time time stamps of stimuli presentations into bins
tone_stop = (tone_stop_p - float(from_where))/window_size
tail_shock = (tail_shock_k - float(from_where))/window_size 

################################ HIPPOCAMPAL NEURONS #################################
### generating spike trains from the hippocampal spike recordings

recording_length_h = 1560.694 # (seconds) recording length is given as the timestemp of the last spiking activity
# (at 1560.694 was the last firing of the 6th neuron)

act_vect_length_h = int(round(recording_length_h)/window_size) # length of the activity vectors
act_mat_hipp = np.zeros((act_vect_length_h, len(hipp_indexes)), dtype=int) # creating the activity matrix with zero values

for neuron_n_h in range(0,len(hipp_indexes)): # iterating through the neurons (columns)
    number_of_h_spikes = 0 # number of the spikes within a time window, of a given hippocampal neuron
    window_j_h = 1 # initial window end
    spike_k_h = 0 # number of the spikes for a given hippocampal neuron
    while np.logical_not(np.isnan(hippocampal_spikes[spike_k_h,neuron_n_h])): # iterating while there is no NaN value
        if hippocampal_spikes[spike_k_h][neuron_n_h] <= window_j_h*window_size: # if the spike firing time is lower than the time window
            number_of_h_spikes += 1 # increase the number of spikes in that given time window with 1
            spike_k_h += 1 # go to the next spike
        else:
            act_mat_hipp[window_j_h-1,neuron_n_h] = number_of_h_spikes # save the number of spikes within that time window
            number_of_h_spikes = 0 # zero out the counting container
            window_j_h += 1 # go to the next window

# taking only the segment between the given points
act_vect_length_h_segment = int(round(until_when-from_where)/window_size)
act_mat_hipp_segment = np.zeros((act_vect_length_h_segment, len(hipp_indexes)), dtype=int)
i = 0
start = int(from_where/window_size)
stop = int(until_when/window_size)
for t in range(start,stop):
    act_mat_hipp_segment[i]=act_mat_hipp[t]
    i += 1

act_mat_hipp_segment = act_mat_hipp_segment.T # transpose
# example: if from_where = 50, until_when = 1450, and window_size = 0.1, then the segment matrix will be 14000 bin long, and its 13999th line will match the 14499th line of the original matrix

#dh = pd.DataFrame(act_mat_hipp_segment) # convert the array into a dataframe
#filepath = 'real_data_act_mat_hipp_segment.xlsx' # save the dataframe into an xlsx file
#dh.to_excel(filepath, index=False)  

################################### MEDIAN RAPHE NEURONS ##########################################
### generating spike trains from the median raphe spike recordings

recording_length_mr = 1560.79065 # (seconds) recording length is given as the timestemp of the last spiking activity
# at 1560.79065 was the last firing of cell 8

act_vect_length_mr = int(round(recording_length_mr)/window_size) # length of the activity vectors
act_mat_medr = np.zeros((act_vect_length_mr, len(medr_indexes)), dtype=int) # creating the activity matrix with zero values

for neuron_n_mr in range(0,len(medr_indexes)): # iterating through the neurons (columns)
    number_of_mr_spikes = 0 # number of the spikes within a time window, of a given median raphe neuron
    window_j_mr = 1 # initial window end
    spike_k_mr = 0 # number of the spikes for a given median raphe neuron
    while np.logical_not(np.isnan(medianraphe_spikes[spike_k_mr,neuron_n_mr])): # iterating while there is no NaN value
        if spike_k_mr == 104446: # if the number of spikes is 104446 for a neuron brake because it is the end of that column (implemented only because of the 8th neuron)
            break
        elif medianraphe_spikes[spike_k_mr][neuron_n_mr] <= window_j_mr*window_size: # if the spike firing time is lower than the time window
            number_of_mr_spikes += 1 # increase the number of spikes in that given time window with 1
            spike_k_mr += 1 # go to the next spike
        else:
            act_mat_medr[window_j_mr-1,neuron_n_mr] = number_of_mr_spikes # save the number of spikes within that time window
            number_of_mr_spikes = 0 # zero out the counting container
            window_j_mr += 1 # go to the next window
            
# taking only the segment between the given points            
act_vect_length_mr_segment = int(round(until_when-from_where)/window_size)
act_mat_medr_segment = np.zeros((act_vect_length_mr_segment, len(medr_indexes)), dtype=int)

i = 0
start = int(from_where/window_size)
stop = int(until_when/window_size)
for t in range(start,stop-1):
    act_mat_medr_segment[i]=act_mat_medr[t]
    i += 1
    
act_mat_medr_segment = act_mat_medr_segment.T # transpose

#############################################################################
######################### ASSEMBLY DETECTION ################################
#############################################################################

##################################### HIPPOCAMPUS #####################################
### using Marcenko-Pastur function as null distribution

patterns_hipp,significance_hipp,zact_mat_hipp_segment = assembly.runPatterns(act_mat_hipp_segment,method='ica',nullhyp='mp')

# plotting the results of the PCA-MP distribution based assembly extraction in the hippocampus
plt.rcParams['font.size'] = 16
plt.figure(figsize=(50,10))
for (pi,pattern) in enumerate(patterns_hipp):
        plt.subplot(1,len(patterns_hipp),1+pi)
        lines = plt.stem(pattern)
        plt.setp(lines, 'linewidth', 3)
        plt.xlabel('Neuron #')
        plt.ylabel('Weight [-]')
        plt.grid()
        plt.tight_layout()
plt.suptitle('Assembly pattern estimation using MP distribution')
plt.show()

###################################################################################################################
### estimating the found assemblies' activity over time

assemblyAct_hipp = assembly.computeAssemblyActivity(patterns_hipp,zact_mat_hipp_segment)

plt.rcParams['font.size'] = 60 
beginning_h = 0 #  arbitrary starting point for the plotting (in bins)
ending_h = 53000 # arbitrary end point (in bins)
tone_start_i = tone_start - beginning_h
tone_stop_i = tone_stop - beginning_h
tail_shock_i = tail_shock - beginning_h
assembly_number_h = 3 # selecting an arbitrary assembly
# plotting the activity of the selected assembly:
plt.figure(figsize=(150,30))
plt.plot(assemblyAct_hipp[assembly_number_h-1,beginning_h:ending_h].T,'g', linewidth=4)
plt.title('# '+ str(assembly_number_h) +' assembly activity in the range of: ' + str(beginning_h) + ' - ' + str(ending_h) + ' ms')
plt.xlabel('Time [bin]')
plt.ylabel('Strength [-]') 
plt.ylim(-100,50) # selecting arbitrary y-axis range
x_end = ending_h-beginning_h
plt.xlim(0,x_end)
# plotting the tones and foot shock presentations:
x_t = np.linspace(tone_start_i,tone_stop_i,1000)
y_t = x_t/x_t -20
plt.plot(x_t,y_t,'b.') # plotting the tone representations with blue lines
plt.plot((tail_shock_i[0],tail_shock_i[2],tail_shock_i[4],tail_shock_i[6]),[-15,-15,-15,-15],'ro') # plotting the foot shock presentations with red dots
plt.show()

################################ MEDIAN RAPHE ####################################

patterns_mr,significance_mr,zact_mat_medr_segment = assembly.runPatterns(act_mat_medr_segment,method='ica',nullhyp='mp')

### plotting the detected cell assemblies' pattern estimation in the median raphe
plt.rcParams['font.size'] = 80
fig = plt.figure(figsize=(100,100))

ax1 = fig.add_subplot(331)
lines = ax1.stem(patterns_mr[0,:])
plt.setp(lines, 'linewidth', 7)
sns.despine(top=True, right=True, left=False, bottom=True)
ax1.get_xaxis().set_visible(False)

ax2 = fig.add_subplot(332, sharey=ax1)
lines = ax2.stem(patterns_mr[1,:])
plt.setp(lines, 'linewidth', 7)
ax2.set_axis_off()

ax3 = fig.add_subplot(333, sharey=ax1)
lines = ax3.stem(patterns_mr[2,:])
plt.setp(lines, 'linewidth', 7)
ax3.set_axis_off()

ax4 = fig.add_subplot(334, sharex=ax1)
lines = ax4.stem(patterns_mr[3,:])
plt.setp(lines, 'linewidth', 7)
sns.despine(top=True, right=True, left=False, bottom=True)
ax4.get_xaxis().set_visible(False)
plt.ylabel('Weight [-]')

ax5 = fig.add_subplot(335,sharex = ax2, sharey = ax4)
lines = ax5.stem(patterns_mr[4,:])
plt.setp(lines, 'linewidth', 7)
ax5.set_axis_off()

ax6 = fig.add_subplot(336,sharex = ax3, sharey = ax4)
lines = ax6.stem(patterns_mr[5,:])
plt.setp(lines, 'linewidth', 7)
ax6.set_axis_off()

ax7 = fig.add_subplot(337,sharex = ax1)
lines = ax7.stem(patterns_mr[6,:])
plt.setp(lines, 'linewidth', 7)
sns.despine()

ax8 = fig.add_subplot(338,sharex = ax2, sharey = ax7)
lines = ax8.stem(patterns_mr[7,:])
plt.setp(lines, 'linewidth', 7)
sns.despine(top=True, right=True, left=True, bottom=False)
ax8.get_yaxis().set_visible(False)
plt.xlabel('Neuron #')

ax9 = fig.add_subplot(339,sharex = ax3, sharey = ax7)
lines = ax9.stem(patterns_mr[8,:])
plt.setp(lines, 'linewidth', 7)
sns.despine(top=True, right=True, left=True, bottom=False)
ax9.get_yaxis().set_visible(False)

plt.tight_layout(True)
plt.show()

##############################################################################################################
### estimating the found assemblies' activity over time
assemblyAct_mr = assembly.computeAssemblyActivity(patterns_mr,zact_mat_medr_segment)

beginning_mr = 0 #  arbitrary starting point for the plotting (in bins)
ending_mr = 53000 # arbitrary end point (in bins)
tone_start_j = tone_start - beginning_mr
tone_stop_j = tone_stop - beginning_mr
tail_shock_j = tail_shock - beginning_mr

### plotting together 2 assemblies' activity around the last 8 tones

assembly_number1 = 6
assembly_numner2 = 3
f = 10 # selecting where the plot should start - (seconds before the beginning of the tone presentation)
u = 30 # selecting where the plot should end - (seconds after the end of the tone presentation)
f_t = np.zeros(8)
u_t = np.zeros(8)
s = (8,1000)
X_t = np.zeros(s)
Y_t = np.zeros(s)
for i in range(4,12): # converting real time to bin
    f_t[i-4] = tone_start_j[i] - f/window_size #400
    u_t[i-4] = tone_start_j[i] + u/window_size #1200
    X_t[i-4] = np.linspace(tone_start_j[i],tone_stop_j[i],1000)
    Y_t[i-4] = X_t[i-4]/X_t[i-4] -10


plt.rcParams['font.size'] = 80
fig = plt.figure(figsize=(100,100))
plt.tight_layout()

########  first assembly's activity   ###########
# plotting the activity around the 5th tone
ax1 = fig.add_subplot(8,2,1)
ax1.plot(assemblyAct_mr[assembly_number1-1,round(f_t[0]):round(u_t[0])].T, 'm', linewidth=5)
X = X_t[0] - f_t[0]
plt.plot(X,Y_t[0],'b-', linewidth=5, label='5th tone')
plt.ylim(-20,30)
#plt.legend(framealpha=1, frameon=True, loc=2)
sns.despine(top=True, right=True, left=False, bottom=True)
ax1.get_xaxis().set_visible(False)

# plotting the activity around the 6th tone
ax2 = fig.add_subplot(8,2,3,sharey = ax1)
ax2.plot(assemblyAct_mr[assembly_number1-1,round(f_t[1]):round(u_t[1])].T, 'm', linewidth=5)
X = X_t[1] - f_t[1]
plt.plot(X,Y_t[1],'b-', linewidth=5, label='6th tone')
#plt.legend(framealpha=1, frameon=True, loc=2)
sns.despine(top=True, right=True, left=False, bottom=True)
ax2.get_xaxis().set_visible(False)

# plotting the activity around the 7th tone
ax3 = fig.add_subplot(8,2,5, sharey=ax1)
ax3.plot(assemblyAct_mr[assembly_number1-1,round(f_t[2]):round(u_t[2])].T, 'm', linewidth=5)
X = X_t[2] - f_t[2]
plt.plot(X,Y_t[2],'b-', linewidth=5, label='6th tone')
sns.despine(top=True, right=True, left=False, bottom=True)
ax3.get_xaxis().set_visible(False)

# plotting the activity around the 8th tone
ax4 = fig.add_subplot(8,2,7, sharey=ax1)
ax4.plot(assemblyAct_mr[assembly_number1-1,round(f_t[3]):round(u_t[3])].T, 'm', linewidth=5)
X = X_t[3] - f_t[3]
plt.plot(X,Y_t[3],'b-', linewidth=5, label='6th tone')
sns.despine(top=True, right=True, left=False, bottom=True)
ax4.get_xaxis().set_visible(False)

# plotting the activity around the 9th tone + 1st shock
ax5 = fig.add_subplot(8,2,9,sharey = ax1)
ax5.plot(assemblyAct_mr[assembly_number1-1,round(f_t[4]):round(u_t[4])].T, 'm', linewidth=5)
X = X_t[4] - f_t[4]
plt.plot(X,Y_t[4],'b-', linewidth=5, label='9th tone')
T = tail_shock_i[0] - f_t[4]
ax5.plot(T,[-15],'ro', markersize=25, label='1st shock')
sns.despine(top=True, right=True, left=False, bottom=True)
ax5.get_xaxis().set_visible(False)
plt.ylabel('Strength [-]')

# plotting the activity around the 10th tone + 2nd shock
ax6 = fig.add_subplot(8,2,11,sharey = ax1)
ax6.plot(assemblyAct_mr[assembly_number1-1,round(f_t[5]):round(u_t[5])].T, 'm', linewidth=5)
X = X_t[5] - f_t[5]
ax6.plot(X,Y_t[5],'b-', linewidth=5, label='10th tone')
T = tail_shock_i[2] - f_t[5]
ax6.plot(T,[-15],'ro', markersize=25, label='2nd shock')
sns.despine(top=True, right=True, left=False, bottom=True)
ax6.get_xaxis().set_visible(False)

# plotting the activity around the 11th tone + 3rd shock
ax7 = fig.add_subplot(8,2,13,sharey = ax1)
ax7.plot(assemblyAct_mr[assembly_number1-1,round(f_t[6]):round(u_t[6])].T, 'm', linewidth=5)
X = X_t[6] - f_t[6]
plt.plot(X,Y_t[6],'b-', linewidth=5, label='11th tone')
T = tail_shock_i[4] - f_t[6]
plt.plot(T,[-15],'ro', markersize=25, label='3rd shock')
sns.despine(top=True, right=True, left=False, bottom=True)
ax7.get_xaxis().set_visible(False)

# plotting the activity around the 12th tone + 4th shock
ax8 = fig.add_subplot(8,2,15,sharey = ax1)
plt.plot(assemblyAct_mr[assembly_number1-1,round(f_t[7]):round(u_t[7])].T, 'm', linewidth=5)
X = X_t[7] - f_t[7]
plt.plot(X,Y_t[7],'b-', linewidth=5, label='12th tone')
T = tail_shock_i[6] - f_t[7]
plt.plot(T,[-15],'ro', markersize=25, label='4th shock')
plt.xlabel('Time [bin]')
sns.despine(top=True, right=True, left=False, bottom=True)
plt.tight_layout()

########    other assembly's activity   ###########

bx1 = fig.add_subplot(8,2,2,sharey = ax1)
bx1.plot(assemblyAct_mr[assembly_numner2-1,round(f_t[0]):round(u_t[0])].T, 'g', linewidth=5)
X = X_t[0] - f_t[0]
plt.plot(X,Y_t[0],'b-', linewidth=5, label='5th tone')
#plt.ylim(-40,60)
#plt.legend(framealpha=1, frameon=True, loc=2)
bx1.set_axis_off()

bx2 = fig.add_subplot(8,2,4,sharey = ax1)
bx2.plot(assemblyAct_mr[assembly_numner2-1,round(f_t[1]):round(u_t[1])].T, 'g', linewidth=5)
X = X_t[1] - f_t[1]
plt.plot(X,Y_t[1],'b-', linewidth=5, label='6th tone')
#plt.legend(framealpha=1, frameon=True, loc=2)
bx2.set_axis_off()

bx3 = fig.add_subplot(8,2,6, sharey=ax1)
bx3.plot(assemblyAct_mr[assembly_numner2-1,round(f_t[2]):round(u_t[2])].T, 'g', linewidth=5)
X = X_t[2] - f_t[2]
plt.plot(X,Y_t[2],'b-', linewidth=5, label='6th tone')
bx3.set_axis_off()

bx4 = fig.add_subplot(8,2,8, sharey=ax1)
bx4.plot(assemblyAct_mr[assembly_numner2-1,round(f_t[3]):round(u_t[3])].T, 'g', linewidth=5)
X = X_t[3] - f_t[3]
plt.plot(X,Y_t[3],'b-', linewidth=5, label='6th tone')
bx4.set_axis_off()

bx5 = fig.add_subplot(8,2,10,sharey = ax1)
bx5.plot(assemblyAct_mr[assembly_numner2-1,round(f_t[4]):round(u_t[4])].T, 'g', linewidth=5)
X = X_t[4] - f_t[4]
plt.plot(X,Y_t[4],'b-', linewidth=5, label='9th tone')
T = tail_shock_i[0] - f_t[4]
bx5.plot(T,[-15],'ro', markersize=25, label='1st shock')
bx5.set_axis_off()

bx6 = fig.add_subplot(8,2,12,sharey = ax1)
bx6.plot(assemblyAct_mr[assembly_numner2-1,round(f_t[5]):round(u_t[5])].T, 'g', linewidth=5)
X = X_t[5] - f_t[5]
bx6.plot(X,Y_t[5],'b-', linewidth=5, label='10th tone')
T = tail_shock_i[2] - f_t[5]
bx6.plot(T,[-15],'ro', markersize=25, label='2nd shock')
bx6.set_axis_off()

bx7 = fig.add_subplot(8,2,14,sharey = ax1)
bx7.plot(assemblyAct_mr[assembly_numner2-1,round(f_t[6]):round(u_t[6])].T, 'g', linewidth=5)
X = X_t[6] - f_t[6]
plt.plot(X,Y_t[6],'b-', linewidth=5, label='11th tone')
T = tail_shock_i[4] - f_t[6]
plt.plot(T,[-15],'ro', markersize=25, label='3rd shock')
bx7.set_axis_off()

bx8 = fig.add_subplot(8,2,16,sharey = ax1)
plt.plot(assemblyAct_mr[assembly_numner2-1,round(f_t[7]):round(u_t[7])].T, 'g', linewidth=5)
X = X_t[7] - f_t[7]
plt.plot(X,Y_t[7],'b-', linewidth=5, label='12th tone')
T = tail_shock_i[6] - f_t[7]
plt.plot(T,[-15],'ro', markersize=25, label='4th shock')
plt.xlabel('Time [bin]')
bx8.get_yaxis().set_visible(False)
sns.despine(top=True, right=True, left=True, bottom=False)

plt.tight_layout()
plt.show()

